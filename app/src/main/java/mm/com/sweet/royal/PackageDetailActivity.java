package mm.com.sweet.royal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PackageDetailActivity extends AppCompatActivity implements RecyclerViewAdapter.OnNoteListener {
    RecyclerView recyclerView;
    RecyclerViewAdapter mAdapter;
    ArrayList<String> stringArrayList = new ArrayList<>();
    ArrayList<String> time = new ArrayList<>();

    CoordinatorLayout coordinatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_detail);
        recyclerView = findViewById(R.id.recyclerView);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        populateRecyclerView();



    }
    private void populateRecyclerView() {

        String waybills = getIntent().getStringExtra("waybill");
        Log.d("","waybill"+waybills);
        if (waybills != null) {

            try {


                JSONArray jarray=new JSONArray(waybills);

                for (int hk = 0; hk < jarray.length(); hk++) {
                    JSONObject d = jarray.getJSONObject(hk);
                    stringArrayList.add(d.getString("waybill_no")+"(time:"+d.getString("datetime")+")");
                    time.add(d.getString("from_city"));
//                    JSONArray jarray1 = d.getJSONArray("waybills");

Log.d("","waybill"+d.getString("waybill_no"));

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(),
                    "Please Check internet Connection", Toast.LENGTH_SHORT)
                    .show();

        }
//        stringArrayList.add("Item 1");
//        stringArrayList.add("Item 2");
//        stringArrayList.add("Item 3");
//        stringArrayList.add("Item 4");
//        stringArrayList.add("Item 5");
//        stringArrayList.add("Item 6");
//        stringArrayList.add("Item 7");
//        stringArrayList.add("Item 8");
//        stringArrayList.add("Item 9");
//        stringArrayList.add("Item 10");

        mAdapter = new RecyclerViewAdapter(stringArrayList,time,this);
        recyclerView.setAdapter(mAdapter);


    }

    @Override
    public void OnNoteClick(int position) {

    }
}
