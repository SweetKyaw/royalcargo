package mm.com.sweet.royal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class SettingActivity extends AppCompatActivity {
    EditText mOldPass,mNewPass,mConfirmPass;
    Button mBtn;
    private String status,msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        mOldPass=(EditText) findViewById(R.id.oldpass);
        mNewPass=(EditText) findViewById(R.id.newpass);
        mConfirmPass=(EditText) findViewById(R.id.confirmpass);

        mBtn=(Button)findViewById(R.id.button1);
//{
//	"user_id":"5",
//	"old_password":"12345678",
//	"new_password":"88888888"
//}
    }
    public void Login(View v){
        final String old_password=mOldPass.getText().toString();
        final String new_password=mNewPass.getText().toString();

        Log.d("","old"+old_password);
        Log.d("","new"+new_password);

        final ProgressDialog pd=new ProgressDialog(SettingActivity.this);
        pd.setTitle("Please Wait");
        pd.setMessage("Connecting to Server");
        pd.show();
        new Thread(new Runnable() {
            @Override


            public void run() {
                HttpClient httpclient=new DefaultHttpClient();
                HttpPost httpGet=new HttpPost("http://royalx.biz/cargo/api/changed-password");
                String user_id = getIntent().getStringExtra("ID");

                List<NameValuePair> postData=new ArrayList<NameValuePair>(3);
                postData.add(new BasicNameValuePair("user_id",user_id));

                postData.add(new BasicNameValuePair("old_password",old_password));
                postData.add(new BasicNameValuePair("new_password",new_password));
////{"success":0}


                Log.d("","Name Value Pair"+postData);
                try{
                    httpGet.setEntity(new UrlEncodedFormEntity(postData));
                }catch (UnsupportedEncodingException e){
                    e.printStackTrace();
                }
                try{
                    HttpResponse response=httpclient.execute(httpGet);
                    HttpEntity entity=response.getEntity();
                    String jsonstr= EntityUtils.toString(entity,"UTF-8");
                    Log.d("","Response"+jsonstr);

                    if (jsonstr != null) {
                        try {
                            JSONObject uid=new JSONObject(jsonstr);
                            status = uid.getString("success");
                            msg = uid.getString("message");
                            Log.d("", "Msg" + msg);

                            if (status != "0") {


                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pd.dismiss();

                                        ViewGroup viewGroup = findViewById(android.R.id.content);

                                        //then we will inflate the custom alert dialog xml that we created
                                        View dialogView = LayoutInflater.from(SettingActivity.this).inflate(R.layout.success_dialog, viewGroup, false);
                                        TextView text = (TextView) dialogView.findViewById(R.id.tv1);
                                        text.setText(msg);
                                        //Now we need an AlertDialog.Builder object
                                        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);

                                        //setting the view of the builder to our custom view that we already inflated
                                        builder.setView(dialogView);

                                        builder.setPositiveButton("OK", null);
                                        //finally creating the alert dialog and displaying it
                                        AlertDialog alertDialog = builder.create();
                                        alertDialog.show();
                                        mOldPass.clearComposingText();

//
//                                         Intent in=new Intent(SettingActivity.this,MainActivity.class);
//                                                    startActivity(in);
//                                                    finish();
                                    }


                                });

                            } else {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pd.dismiss();
                                        AlertDialog dialog = new AlertDialog.Builder(SettingActivity.this).create();
                                        dialog.setTitle("");
                                        dialog.setMessage("Error");
                                        dialog.show();

                                    }
                                });
                            }




                        } catch (final JSONException e) {
                            Log.e("", "Json parsing error: " + e.getMessage());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pd.dismiss();


                                        }
                                    });

                                }
                            });

                        }

                    } else {
                        Log.e("", "Couldn't get json from server.");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                Toast.makeText(getApplicationContext(),
//                                        "Couldn't get json from server. Check LogCat for possible errors!",
//                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }


                } catch(ClientProtocolException ce){
                    Log.d("","Protocol Exception"+ce.toString());

                }catch(IOException ie){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();

                        }
                    });
                    Log.d("","IOException"+ie.toString());
                }


            }
        }).start();
    }

}

