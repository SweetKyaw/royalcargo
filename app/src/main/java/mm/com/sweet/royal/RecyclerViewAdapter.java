package mm.com.sweet.royal;

//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>  {

    private ArrayList<String> data;
    private ArrayList<String> data1;

    private OnNoteListener mOnNotelistener;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTitle,mTitle1;
        private ImageView mImg;
        RelativeLayout relativeLayout;
OnNoteListener onNoteListener;
        public MyViewHolder(View itemView, OnNoteListener onNoteListener) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.txtTitle);
            mTitle1=itemView.findViewById(R.id.txtTitle1);
            mImg=itemView.findViewById(R.id.imageView);
            this.onNoteListener=onNoteListener;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onNoteListener.OnNoteClick(getAdapterPosition());
        }
    }


public interface OnNoteListener{
        void OnNoteClick(int position);
}


    public RecyclerViewAdapter(ArrayList<String> data,ArrayList<String> data1,OnNoteListener onNoteListener) {
        this.data = data;
        this.data1=data1;
        this.mOnNotelistener=onNoteListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_row, parent, false);
        return new MyViewHolder(itemView,mOnNotelistener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.mTitle.setText(data.get(position));
        holder.mTitle1.setText("From_City:"+data1.get(position));

    }


    @Override
    public int getItemCount() {
        return data.size();

    }


    public void removeItem(int position) {
        data.remove(position);
        data1.remove(position);

        notifyItemRemoved(position);
    }

    public void restoreItem(String item,String item1, int position) {
        data.add(position, item);
        data1.add(position, item1);

        notifyItemInserted(position);
    }

    public ArrayList<String> getData() {
        return data;
    }
    public ArrayList<String> getData1() {
        return data1;
    }
}

