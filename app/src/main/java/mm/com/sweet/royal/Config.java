package mm.com.sweet.royal;

/**
 * Created by arkar on 5/17/16.
 */
public class Config {

    //Live API
//    public static final String API_URL = "http://network.royalx.biz";
    //public static final String API_URL = "http://uat.royalx.biz";
//    public static final String API_PATH = "/royalx/";
    public static final String API_PATH = "/royalx/";

    // API OBJECT
//    public static final String API_OBJECT = "royalx";
//    public static final String PICKUP_METHOD = "pickup";
//    public static final String CONSIGNMENT_METHOD = "consignment";
//    public static final String TRANSHIPMENT_METHOD = "transhipment";
//    public static final String USER_METHOD = "users";
//    public static final String CITY_METHOD = "city";
//    public static final String TOWNSHIP_METHOD = "township";
//    public static final String EXPRESS_METHOD = "express";
//    public static final String CUSTOMER_REQUEST_METHOD = "pickup.customerrequest";
//    public static final String POSTPONE_REASON_METHOD = "postponereason";

//    public static final int SIGNATURE_WIDTH = 300;
//    public static final int SIGNATURE_HEIGHT = 100;

//
//    public static final String[] DEMO_CUSTOMER_REQUEST = {
//        "Customer Request_1",
//        "Customer Request_1"
//    };

//    public static  public static final int STATUS_FAIL = 0;
    //    public static final int STATUS_SUCCESS = 1;
//    public static final int STATUS_PENDING = 2;
//
//    public static final String PICK_STATUS_PENDING = "pending";
//    public static final String PICK_STATUS_ASSIGNED = "assigned";
//    public static final String PICK_STATUS_FINISHED = "finished";
//    final int MAX_ITEMS_PER_REQUEST = 5;
//
//    public static final int DEFAULT_STOKE_SIZE = 12;
//
//    public static final String TO_DELIVER_TRANSHIPMENT = "OUT";
//    public static final String TO_PICK_TRANSHIPMENT = "IN";
//
//    public static final String SYNC_TYPE_UPDATE_PICKUP = "pickup.update";
//    public static final String SYNC_TYPE_CANCEL_PICKUP = "pickup.cancel";
//    public static final String SYNC_TYPE_UPDATE_CONSIGNMENT = "consignment.update";
//    public static final String SYNC_TYPE_POSTPONE_CONSIGNMENT = "consignment.postpone";
//    public static final String SYNC_TYPE_UPDATE_TRANSHIPMENT = "transhipment.update";

    public static final String APK_BASE_URL = "http://royalx.biz/cargo/";
    public static final String APK_FILE_NAME = "cargo.apk";
    public static final String APK_DOWNLOAD_URL = APK_BASE_URL + APK_FILE_NAME;

}
