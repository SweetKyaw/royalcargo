package mm.com.sweet.royal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecentBranchinActivity extends AppCompatActivity implements PackagesAdapter.OnNoteListener  {

    RecyclerView recyclerView;
    PackagesAdapter mAdapter;
    ArrayList<String> stringArrayList = new ArrayList<>();
    ArrayList<String> time = new ArrayList<>();

    CoordinatorLayout coordinatorLayout;
    private String pack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_branchin);

        recyclerView = findViewById(R.id.recyclerView);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        populateRecyclerView();




    }

    private void populateRecyclerView() {

        String branchinresponse = getIntent().getStringExtra("binrecent");
        if (branchinresponse != null) {

            try {


                JSONArray jarray=new JSONArray(branchinresponse);

                for (int hk = 0; hk < jarray.length(); hk++) {
                    JSONObject d = jarray.getJSONObject(hk);
                    stringArrayList.add(d.getString("package"));

                    time.add(d.getString("count"));
//count
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(),
                    "Please Check internet Connection", Toast.LENGTH_SHORT)
                    .show();

        }


//        stringArrayList.add("Item 1");
//        stringArrayList.add("Item 2");
//        stringArrayList.add("Item 3");
//        stringArrayList.add("Item 4");
//        stringArrayList.add("Item 5");
//        stringArrayList.add("Item 6");
//        stringArrayList.add("Item 7");
//        stringArrayList.add("Item 8");
//        stringArrayList.add("Item 9");
//        stringArrayList.add("Item 10");

        mAdapter = new PackagesAdapter(stringArrayList,time,this);
        recyclerView.setAdapter(mAdapter);


    }


    @Override
    public void OnNoteClick(int position) {
        Toast.makeText(getApplicationContext(),
                "Click"+stringArrayList.get(position), Toast.LENGTH_SHORT)
                .show();
         pack=stringArrayList.get(position);
        String branchinresponse = getIntent().getStringExtra("binrecent");
        if (branchinresponse != null) {

            try {


                JSONArray jarray=new JSONArray(branchinresponse);

                for (int hk = 0; hk < jarray.length(); hk++) {
                    JSONObject d = jarray.getJSONObject(hk);
                    String pack1=d.getString("package");
                    if(pack.equals(pack1)){
                        JSONArray waybill=d.getJSONArray("waybills");
                        Log.d("","waybill"+waybill);
                        Intent in=new Intent(RecentBranchinActivity.this,PackageDetailActivity.class)     ;
                        in.putExtra("waybill",waybill.toString());
                        startActivity(in);

                    }else{

                    }
//                    stringArrayList.add(d.getString("package"));
//                    JSONArray jarray1 = d.getJSONArray("waybills");



                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getApplicationContext(),
                    "Please Check internet Connection", Toast.LENGTH_SHORT)
                    .show();

        }
    }
}



