package mm.com.sweet.royal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class RecentActivity extends AppCompatActivity {
    private Button mBtn1, mBtn2, mBtn3;
//private TextView mtxt1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent);
        mBtn1 = (Button) findViewById(R.id.branchinrecent);
        mBtn2 = (Button) findViewById(R.id.handoverrecent);
        mBtn3 = (Button) findViewById(R.id.receivedrecent);
//        mtxt1=(TextView)findViewById(R.id.badge_notification_bin);
    }

    public void binrecent(View v){
        final ProgressDialog pd = new ProgressDialog(RecentActivity.this);
        pd.setTitle("Please Wait");
        pd.setMessage("Connecting to Server");
        pd.show();

        new Thread(new Runnable() {
            @Override


            public void run() {


                HttpClient httpclient = new DefaultHttpClient();
                //http://royalx.biz/cargo/api/branch-action
                HttpPost httpGet = new HttpPost("http://royalx.biz/cargo/api/recent-packages");
                String userid = getIntent().getStringExtra("ID");
                int i;
//            Log.d("", "UserID" + arrayList.toString());
                //{
                //	"user_id":"3",
                //	"status":"branch-in"
                //}
                List<NameValuePair> postData = new ArrayList<NameValuePair>(2);
                postData.add(new BasicNameValuePair("user_id", userid));
                postData.add(new BasicNameValuePair("status", "branch-in"));

                Log.d("", "Name Value Pair" + postData);

                try {
                    httpGet.setEntity(new UrlEncodedFormEntity(postData));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    HttpResponse response = httpclient.execute(httpGet);

                    HttpEntity entity = response.getEntity();
                    final String responseString = EntityUtils.toString(entity);
                    Log.d("", "Response" +responseString);
                    if (responseString != null) {
//                        Integer count=responseString.length();
//                        mtxt1.setText(count);
                        pd.dismiss();
// JSONObject res = new JSONObject(responseString);
                        Intent in=new Intent(RecentActivity.this,RecentBranchinActivity.class);
                        in.putExtra("binrecent",responseString);
                        startActivity(in);


                    } else {
                        Log.e("", "Couldn't get json from server.");

                    }


                } catch (ClientProtocolException ce) {
                    Log.d("", "Protocol Exception" + ce.toString());

                } catch (IOException ie) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                        pd.dismiss();
                            AlertDialog dialog = new AlertDialog.Builder(RecentActivity.this).create();
                            dialog.setTitle("NoConnection");
                            dialog.setMessage("Please check yur internet");
                            dialog.show();
                        }
                    });
                    Log.d("", "IOException" + ie.toString());
                }

//               }
            }
        }).start();
    }
//    //
//    handoverrecent
//            receivedrecent
//

    public void handoverrecent(View v){
        final ProgressDialog pd = new ProgressDialog(RecentActivity.this);
        pd.setTitle("Please Wait");
        pd.setMessage("Connecting to Server");
        pd.show();

        new Thread(new Runnable() {
            @Override


            public void run() {


                HttpClient httpclient = new DefaultHttpClient();
                //http://royalx.biz/cargo/api/branch-action
                HttpPost httpGet = new HttpPost("http://royalx.biz/cargo/api/recent-packages");
                String userid = getIntent().getStringExtra("ID");
                int i;
//            Log.d("", "UserID" + arrayList.toString());
                //{
                //	"user_id":"3",
                //	"status":"branch-in"
                //}
                List<NameValuePair> postData = new ArrayList<NameValuePair>(2);
                postData.add(new BasicNameValuePair("user_id", userid));
                postData.add(new BasicNameValuePair("status", "handover"));

                Log.d("", "Name Value Pair" + postData);

                try {
                    httpGet.setEntity(new UrlEncodedFormEntity(postData));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    HttpResponse response = httpclient.execute(httpGet);

                    HttpEntity entity = response.getEntity();
                    final String responseString = EntityUtils.toString(entity);
                    Log.d("", "Response" +responseString);
                    if (responseString != null) {
                        pd.dismiss();
// JSONObject res = new JSONObject(responseString);
                        Intent in=new Intent(RecentActivity.this,RecentBranchinActivity.class);
                        in.putExtra("binrecent",responseString);
                        startActivity(in);


                    } else {
                        Log.e("", "Couldn't get json from server.");

                    }


                } catch (ClientProtocolException ce) {
                    Log.d("", "Protocol Exception" + ce.toString());

                } catch (IOException ie) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                        pd.dismiss();
                            AlertDialog dialog = new AlertDialog.Builder(RecentActivity.this).create();
                            dialog.setTitle("NoConnection");
                            dialog.setMessage("Please check yur internet");
                            dialog.show();
                        }
                    });
                    Log.d("", "IOException" + ie.toString());
                }

//               }
            }
        }).start();
    }
    public void receivedrecent(View v){
        final ProgressDialog pd = new ProgressDialog(RecentActivity.this);
        pd.setTitle("Please Wait");
        pd.setMessage("Connecting to Server");
        pd.show();

        new Thread(new Runnable() {
            @Override


            public void run() {


                HttpClient httpclient = new DefaultHttpClient();
                //http://royalx.biz/cargo/api/branch-action
                HttpPost httpGet = new HttpPost("http://royalx.biz/cargo/api/recent-packages");
                String userid = getIntent().getStringExtra("ID");
                int i;
//            Log.d("", "UserID" + arrayList.toString());
                //{
                //	"user_id":"3",
                //	"status":"branch-in"
                //}
                List<NameValuePair> postData = new ArrayList<NameValuePair>(2);
                postData.add(new BasicNameValuePair("user_id", userid));
                postData.add(new BasicNameValuePair("status", "received"));

                Log.d("", "Name Value Pair" + postData);

                try {
                    httpGet.setEntity(new UrlEncodedFormEntity(postData));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    HttpResponse response = httpclient.execute(httpGet);

                    HttpEntity entity = response.getEntity();
                    final String responseString = EntityUtils.toString(entity);
                    Log.d("", "Response" +responseString);
                    if (responseString != null) {
                        pd.dismiss();
// JSONObject res = new JSONObject(responseString);
                        Intent in=new Intent(RecentActivity.this,RecentBranchinActivity.class);
                        in.putExtra("binrecent",responseString);
                        startActivity(in);


                    } else {
                        Log.e("", "Couldn't get json from server.");

                    }


                } catch (ClientProtocolException ce) {
                    Log.d("", "Protocol Exception" + ce.toString());

                } catch (IOException ie) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                        pd.dismiss();
                            AlertDialog dialog = new AlertDialog.Builder(RecentActivity.this).create();
                            dialog.setTitle("NoConnection");
                            dialog.setMessage("Please check yur internet");
                            dialog.show();
                        }
                    });
                    Log.d("", "IOException" + ie.toString());
                }

//               }
            }
        }).start();
    }

}









