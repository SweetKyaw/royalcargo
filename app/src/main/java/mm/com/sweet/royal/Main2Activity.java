package mm.com.sweet.royal;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.os.Environment;
import android.util.Log;
import android.view.View;

import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import mm.com.sweet.royal.network.UpdateCheckerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
        private CardView mCard1,mCard2,mCard3,mCard4;
        private TextView mtv1,mtv2;
        public final String PREFS_NAME="MyPrefsFile";
    private ProgressDialog mProgressDialog;

    private Button mBtn1, mBtn2;
    private String userid,type,city,branch,username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        checkAndRequestExterStoragePermission();



        mBtn1=(Button) findViewById(R.id.inbound);
        mBtn2=(Button) findViewById(R.id.outbound);
        city=getIntent().getStringExtra("city");
        userid=getIntent().getStringExtra("ID");
        type=getIntent().getStringExtra("type");
        branch=getIntent().getStringExtra("branch");
        username=getIntent().getStringExtra("username");

        //Button Declare
//mCard1=(CardView)findViewById(R.id.cardbranchin) ;
//mCard2=(CardView)findViewById(R.id.cardHandover);
//mCard3=(CardView)findViewById(R.id.cardReceived) ;
//        mCard4=(CardView)findViewById(R.id.cardreport) ;
//
//mCard1.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View v) {
//        Intent in1=new Intent(Main2Activity.this,MainSubActivity.class);
//        in1.putExtra("ID",userid);
//        startActivity(in1);
//    }
//});
//
//        mCard2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//        Intent in2=new Intent(Main2Activity.this,HandoverActivity.class);
//        in2.putExtra("city",city);
//
//        in2.putExtra("ID",userid);
//
//        startActivity(in2);
//
//            }
//        });
//
//
//        mCard3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//        Intent in3=new Intent(Main2Activity.this,ReceieveActivity.class);
//        in3.putExtra("ID",userid);
//
//        startActivity(in3);
//
//            }
//        });
//
////        mRecent=(Button)findViewById(R.id.recent);
//
        city=getIntent().getStringExtra("city");
        userid=getIntent().getStringExtra("ID");
        type=getIntent().getStringExtra("type");
        Log.d("","Main2Activity"+type);
        Integer id = Integer.parseInt(type);

if(id.equals(3)){
    mBtn1.setVisibility(View.GONE);
}
else {

}

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


    }

    //button click
//    public void branchin(View v1){
//
//        Intent in1=new Intent(Main2Activity.this,MainSubActivity.class);
//        in1.putExtra("ID",userid);
//        startActivity(in1);
//    }
//
//
//    public void handover(View v2){
//        Intent in2=new Intent(Main2Activity.this,HandoverActivity.class);
//        in2.putExtra("city",city);
//
//        in2.putExtra("ID",userid);
//
//        startActivity(in2);
//    }
//    public void receieve(View v3){
//        Intent in3=new Intent(Main2Activity.this,ReceieveActivity.class);
//        in3.putExtra("ID",userid);
//
//        startActivity(in3);
//    }


    public void inbound(View v) {

        Intent in=new Intent (Main2Activity.this,OperatorMainActivity.class);
        in.putExtra("ID", userid);
        in.putExtra("branch", branch);
        in.putExtra("city", city);
        in.putExtra("username", username);
        in.putExtra("type", type);
        startActivity(in);

    }

    public void outbound(View v) {

        Intent in=new Intent (Main2Activity.this,InChargeTaskActivity.class);
        in.putExtra("ID", userid);
        in.putExtra("branch", branch);
        in.putExtra("city", city);
        in.putExtra("username", username);
        in.putExtra("type", type);
        startActivity(in);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        String Branch=getIntent().getStringExtra("branch");
        Log.d("","Main2Activity"+Branch);
        String City=getIntent().getStringExtra("city");
        Log.d("","Main2Activity"+City);
        String username=getIntent().getStringExtra("username");
        mtv1=(TextView) findViewById(R.id.tv1);
        mtv1.setText(username);
        mtv2=(TextView) findViewById(R.id.textView);
        mtv2.setText(Branch);



        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }
//{
//    "user_id": 4,
//    "branch_id": "2",
//    "username": "Hnin Yee Mg",
//    "branch": "YGN-48",
//    "city": "YGN",
//    "type": null,
//    "success": 1
//}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent in=new Intent(Main2Activity.this,MainActivity.class);
            startActivity(in);
            finish();
            // Handle the camera action
        } else if (id == R.id.nav_setting) {
            Intent in1 = new Intent(Main2Activity.this, SettingActivity.class);
            in1.putExtra("ID", userid);

            startActivity(in1);
        }else if(id==R.id.nav_recent){
            Intent in1 = new Intent(Main2Activity.this, RecentActivity.class);
            in1.putExtra("ID", userid);

            startActivity(in1);
        } else if (id == R.id.nav_download_update) {
            checkNewVersion();
            // declare the dialog as a member field of your activity
//            ProgressDialog mProgressDialog;
//
//// instantiate it within the onCreate method
//            mProgressDialog = new ProgressDialog(Main2Activity.this);
//            mProgressDialog.setMessage("A message");
//            mProgressDialog.setIndeterminate(true);
//            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            mProgressDialog.setCancelable(true);
//
//// execute this when the downloader must be fired
//            final DownloadTask downloadTask = new DownloadTask(Main2Activity.this);
//            downloadTask.execute("http://royalx.biz/cargo/cargo.apk");
//
//            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//
//                @Override
//                public void onCancel(DialogInterface dialog) {
//                    downloadTask.cancel(true); //cancel the task
//                }
//            });
        }
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_tools) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void checkAndRequestExterStoragePermission() {
        MarshMallowPermissionUtils permissionUtils = new MarshMallowPermissionUtils(this);
        if ((!permissionUtils.checkPermissionForExternalStorage()) || (!permissionUtils.checkPermissionForCamera()) ||
                (!permissionUtils.checkPermissionForRecord())) {
            //  permissionUtils.requestPermissionForExternalStorage();
            permissionUtils.requestPermissionForAll();
        }
    }
    private void checkNewVersion() {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.label_wait),
                getString(R.string.message_checking_new_version));

        UpdateCheckerService updateCheckerService = NetworkProvider
                .getRestAdapter(Config.APK_BASE_URL).create(UpdateCheckerService.class);
        Call<CheckUpdateResponse> call = updateCheckerService.checkUpdate();
        call.enqueue(new Callback<CheckUpdateResponse>() {
            @Override
            public void onResponse(Call<CheckUpdateResponse> call, Response<CheckUpdateResponse> response) {
                hideProgressDialog();
                downloadNewVersion(Config.APK_DOWNLOAD_URL, Config.APK_FILE_NAME);

                if (response.isSuccessful()) {
                    CheckUpdateResponse checkUpdateResponse = response.body();
                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager()
                                .getPackageInfo(getPackageName(), 0);
                        final int currentVersionCode = pInfo.versionCode;
                        if (checkUpdateResponse.getAppVersion() > currentVersionCode) {
                            showMessage(getString(R.string.message_start_downloading));
                            downloadNewVersion(Config.APK_DOWNLOAD_URL, Config.APK_FILE_NAME);
                        } else {
                            String latestMsg = getString(R.string.message_up_to_date);
                            latestMsg = String.format(latestMsg, pInfo.versionName);
                            showMessage(latestMsg);
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                        showMessage(getString(R.string.error_unable_to_check_version));
                    }
                } else {
                    showMessage(getString(R.string.error_unable_to_check_version));
                }
            }

            @Override
            public void onFailure(Call<CheckUpdateResponse> call, Throwable t) {
                hideProgressDialog();
                showMessage(getString(R.string.error_unable_to_check_version));
            }
        });
    }

    private void downloadNewVersion(String url, String fileName) {
        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        //Delete update file if exists
        final File file = new File(destination);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(getString(R.string.label_downloading_update));
        request.setTitle(getString(R.string.app_name));

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                Intent install = new Intent(Intent.ACTION_VIEW);
                install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (Build.VERSION.SDK_INT >= 24) { // Nougat and above
                    Uri apkURI = FileProvider.getUriForFile(
                            Main2Activity.this,
                            getApplicationContext().getPackageName() + ".provider", file);
                    install.setDataAndType(apkURI, "application/vnd.android.package-archive");
                    install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                } else {
                    install.setDataAndType(uri, "application/vnd.android.package-archive");
                }
                startActivity(install);
                unregisterReceiver(this);
                finish();
            }
        };
        //register receiver for when .apk download is compete
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }
    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

}
