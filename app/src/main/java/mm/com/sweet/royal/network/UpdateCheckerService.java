package mm.com.sweet.royal.network;


import mm.com.sweet.royal.CheckUpdateResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by gca on 4/9/18.
 */

public interface UpdateCheckerService {

    @GET("version-control.json")
    Call<CheckUpdateResponse> checkUpdate();
}
