package mm.com.sweet.royal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.google.android.gms.vision.barcode.Barcode;
import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;
import com.notbytes.barcode_reader.BarcodeReaderActivity;
import com.notbytes.barcode_reader.BarcodeReaderFragment;
import com.notbytes.barcode_reader.camera.CameraSource;
import com.notbytes.barcode_reader.camera.CameraSourcePreview;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;

public class MainSubActivity extends AppCompatActivity implements  BarcodeReaderFragment.BarcodeReaderListener {
    private BarcodeReaderFragment barcodeReader;
    private Barcode barcodeResult;
    private TextView txt;
    //    private TextView mtv1;
//
  CheckBox mCh;
//    StringBuffer handover = new StringBuffer();


    private String status, msg;
    private ArrayAdapter arrayAdapter;
    private ListView mlist;
    private String listItem,keyinitem,handover;
    private String branch;
    private String branchid;
    private FrameLayout mframe;
    //    private Integer a;
    ArrayList<String> arrayList = new ArrayList<String>();
    ArrayList<String> arrayList1 = new ArrayList<String>();
    ArrayList<String> arrayList2 = new ArrayList<String>();
    ArrayList<String> arrayList3 = new ArrayList<String>();
    ArrayList<String> arrayList4 = new ArrayList<String>();
    JSONArray townships = new JSONArray();
    //    JSONArray blist = new JSONArray();
    private Button mBtn;
    //    JSONArray bdata = new JSONArray(arrayList);
//
    LinkedHashSet<String> hashSet = new LinkedHashSet<>();
//    public final String PREFS_NAME="MyPrefFile";
private String result;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sub);
//        findViewById(R.id.fab).setOnClickListener(this);
        mframe = (FrameLayout) findViewById(R.id.fm_container);
        new MainSubActivity.GetTownships().execute();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        txt = (TextView) findViewById(R.id.badge_notification_1);
        mBtn = (Button) findViewById(R.id.post);
        setSupportActionBar(toolbar);
        mlist = (ListView) findViewById(R.id.listview);
mCh=(CheckBox)findViewById(R.id.checkBox1);





        mCh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // update your model (or other business logic) based on isChecked
                if(mCh.isChecked()) {
                    mCh.setChecked(true);

                }else{
                    mCh.setChecked(false);

                }
            }
        });




        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBarcodeReaderFragment();
//
            }
        });




        final FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LayoutInflater li = LayoutInflater.from(MainSubActivity.this);
                View promptsView = li.inflate(R.layout.keyin, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MainSubActivity.this);




                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        keyinitem=userInput.getText().toString();
                                        char firstkey=keyinitem.charAt(0);
                                        String k=String.valueOf(firstkey);
                                        if(k.matches("[A-Z]")) {
                                            final Integer blength = keyinitem.length();
                                            if (blength.equals(11)) {
                                                String bdata = keyinitem.substring(1, 10);
                                                String regex = "\\d+";
                                                if (bdata.matches(regex)) {
                                                    arrayList.add(keyinitem);
                                                    Log.d("","keyin"+arrayList);

                                                    final Integer count= arrayList.size();
                                                    if(count>0 && count<=25){
                                                        txt.setText(count.toString());
                                                        hashSet.addAll(arrayList);
                                                        arrayList.clear();
                                                        arrayList.addAll(hashSet);

                                                        Collections.reverse(arrayList);

                                                        Log.d("","Item"+arrayList);

                                                        arrayAdapter.notifyDataSetChanged();
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                mBtn.setBackgroundColor(getResources().getColor(R.color.branchin));

                                                                mBtn.setClickable(true);
                                                                if(count.equals(25)){
                                                                    AlertDialog dialog = new AlertDialog.Builder(MainSubActivity.this).create();
                                                                    dialog.setTitle("Maximum Limit");
                                                                    dialog.setMessage("25");
                                                                    dialog.show();
                                                                }

                                                            }
                                                        });


                                                    }else if(count>25){


                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                mBtn.setBackgroundColor(getResources().getColor(R.color.branchin));


                                                                mBtn.setClickable(true);
                                                                arrayList.remove( arrayList.size() - 1 );
                                                                AlertDialog dialog = new AlertDialog.Builder(MainSubActivity.this).create();
                                                                dialog.setTitle("Maximum Limit");
                                                                dialog.setMessage("25");
                                                                dialog.show();
                                                            }
                                                        });


                                                    }else{

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                mBtn.setClickable(false);

                                                            }
                                                        });





                                                    }

                                                }
                                            }
                                        }else{

                                        }

                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

        });

    }

    //get data from server
    private class GetTownships extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected Void doInBackground(Void... arg0) {
//            HttpHandler sh = new HttpHandler();
            HttpClient client = new DefaultHttpClient();
//https://af8f0f14.ngrok.io/api
            //http://royalx.biz/cargo/api/branches
            // Making a request to url and getting response
            String url = "http://royalx.biz/cargo/api/branches";
            HttpGet request = new HttpGet("http://royalx.biz/cargo/api/branches");
            HttpResponse response;
            try {
                response = client.execute(request);
                HttpEntity entity=response.getEntity();
                String jsonstr= EntityUtils.toString(entity,"UTF-8");
                Log.e("", "Response from url: " + jsonstr);
                if (jsonstr != null) {
                    try {
                        townships = new JSONArray(jsonstr);
                        for (int i = 0; i < townships.length(); i++) {
                            JSONObject c = townships.getJSONObject(i);

                            String type=c.getString("type");


                            if(type.equals("1")){
                                String name = c.getString("branch");
                                Log.d("","Type1"+name);
                                arrayList1.add(name);
                                String id = c.getString("id");

                                arrayList3.add(id);
                                Log.d("","Arraylist3"+arrayList3);
                            }else{

                            }

                        }
                    } catch (final JSONException e) {
                        Log.e("", "Json parsing error: " + e.getMessage());

                    }

                } else {
                    Log.e("", "Couldn't get json from server.");

                }
            } catch (ClientProtocolException ce) {
                ce.printStackTrace();
            } catch (IOException ie) {
                ie.printStackTrace();
            }




            return null;
        }


        @Override
        protected void onPostExecute(Void args){

            arrayAdapter = new ArrayAdapter<String>(MainSubActivity.this, android.R.layout.simple_list_item_1,
                    android.R.id.text1,arrayList);
            mlist.setAdapter(arrayAdapter);


 SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        mlist,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    arrayList.remove(position);
                                    hashSet.removeAll(hashSet);

                                    arrayAdapter.notifyDataSetChanged();
                                    final Integer count = arrayList.size();
                                    txt.setText(count.toString());

                                }

                            }
                        });
        mlist.setOnTouchListener(touchListener);


            Log.d("","ArrayList"+arrayList1)          ;

            Spinner mySpinner = (Spinner) findViewById(R.id.spinner);

            // Spinner adapter
            mySpinner
                    .setAdapter(new ArrayAdapter<String>(MainSubActivity.this,
                            android.R.layout.simple_spinner_dropdown_item,
                            arrayList1));

            // Spinner on item click listener
            mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    branch = adapterView.getItemAtPosition(i).toString();
                    Log.d("","IDD"+l);
                    branchid=arrayList3.get(i);
                    Log.d("","BranchID"+branchid);
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }


    //call barcode fragment
    private void addBarcodeReaderFragment() {
        BarcodeReaderFragment readerFragment = BarcodeReaderFragment.newInstance(true, false, View.VISIBLE);
        readerFragment.setListener(this);
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fm_container, readerFragment);
        fragmentTransaction.commitAllowingStateLoss();

    }



    //Result & List binding
    @Override
    public void onScanned(Barcode barcode) {
        Log.e("", "onScanned: " + hashSet);
        listItem=barcode.rawValue;
char first=listItem.charAt(0);
        String s=String.valueOf(first);
//[a-zA-Z]
        if(s.matches("[A-Z]")){
            final Integer  blength=listItem.length();
            if(blength.equals(11)){
                String bdata=listItem.substring(1, 10);
                String regex = "\\d+";
                if(bdata.matches(regex)){
                    arrayList.add(listItem);
//                    hashSet.addAll(arrayList);
//                    arrayList.clear();
//                    arrayList.addAll(hashSet);
//
//                    Collections.reverse(arrayList);
//
//                    Log.d("","Item"+arrayList);
//
//                    arrayAdapter.notifyDataSetChanged();

                    final Integer count= arrayList.size();
//                    txt.setText(count.toString());
                    if(count>0 && count<=25){
                        txt.setText(count.toString());

                        hashSet.addAll(arrayList);
                        arrayList.clear();
                        arrayList.addAll(hashSet);

                        Collections.reverse(arrayList);

                        Log.d("","Item"+arrayList);

                        arrayAdapter.notifyDataSetChanged();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mBtn.setBackgroundColor(getResources().getColor(R.color.branchin));

                                mBtn.setClickable(true);
                                if(count.equals(25)){
                                    AlertDialog dialog = new AlertDialog.Builder(MainSubActivity.this).create();
                                    dialog.setTitle("Maximum Limit");
                                    dialog.setMessage("25");
                                    dialog.show();
                                }else{

                                }
                            }
                        });

// namesList.removeIf( name -> name.equals("alex"));
//
                    }else if(count>25){


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//

                                mBtn.setBackgroundColor(getResources().getColor(R.color.branchin));

                                arrayList.remove( arrayList.size() - 1 );
//                                final Integer count= arrayList.size();
//                                txt.setText(count.toString());

                                mBtn.setClickable(true);
//                                AlertDialog dialog = new AlertDialog.Builder(MainSubActivity.this).create();
//                                dialog.setTitle("Maximum Limit");
//                                dialog.setMessage("25");
//                                dialog.show();
//                    onDestroy();
                            }
                        });


                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mBtn.setClickable(false);

                            }
                        });





                    }

                }
            }
        }else{

        }



    }


    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {

    }

    @Override
    public void onCameraPermissionDenied() {

    }



//post data to server
    public void post(View v) {
        final ProgressDialog pd = new ProgressDialog(MainSubActivity.this);
        pd.setTitle("Please Wait");
        pd.setMessage("Connecting to Server");
        pd.show();

        if (arrayList.size() > 0 ) {



        new Thread(new Runnable() {
            @Override


            public void run() {


                HttpClient httpclient = new DefaultHttpClient();
                //http://royalx.biz/cargo/api/branch-action
                HttpPost httpGet = new HttpPost("http://royalx.biz/cargo/api/branch-action");
                String userid = getIntent().getStringExtra("ID");
                int i;
                Log.d("", "UserID" + arrayList.toString());
                if(mCh.isChecked()){

                    handover="1";
                }else{

                    handover="0";

                }
                List<NameValuePair> postData = new ArrayList<NameValuePair>(4);
                postData.add(new BasicNameValuePair("action", "branch-in"));
                postData.add(new BasicNameValuePair("from_branch", branchid));
                postData.add(new BasicNameValuePair("user_id", userid));
                postData.add(new BasicNameValuePair("handover", handover));

                postData.add(new BasicNameValuePair("waybills", arrayList.toString()));

                Log.d("", "Name ValuePair" + postData);

                try {
                    httpGet.setEntity(new UrlEncodedFormEntity(postData));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    HttpResponse response = httpclient.execute(httpGet);

                    HttpEntity entity = response.getEntity();
                    final String responseString = EntityUtils.toString(entity, "UTF-8");
                    Log.d("", "Response" + responseString);
                    if (responseString != null) {
                        try {
                            JSONObject res = new JSONObject(responseString);
                            status = res.getString("success");
                            msg = res.getString("message");
                            Log.d("", "Msg" + msg);

                            if (status != "0") {
//             \\
//             \ onRestart();


                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pd.dismiss();

//                                        ViewGroup viewGroup = findViewById(android.R.id.content);
ViewGroup viewGroup=findViewById(android.R.id.content);
                                        //then we will inflate the custom alert dialog xml that we created
                                        View dialogView = LayoutInflater.from(MainSubActivity.this).inflate(R.layout.success_dialog, viewGroup, false);
                                        TextView text = (TextView) dialogView.findViewById(R.id.tv1);
//                                        String styledText = "<span style=\"color:#FF0000;\">Hello</span>";
                                        text.setText(Html.fromHtml(msg), TextView.BufferType.SPANNABLE);
//                                        text.setText(Html.fromHtml(welcomStr));
//                                        text.setTextColor(getResources().getColor(R.color.danger));
                                        //<span class="danger">1</span> ,saved:<span class="secondary">0
                                        //Now we need an AlertDialog.Builder object
                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainSubActivity.this);

                                        //setting the view of the builder to our custom view that we already inflated
                                        builder.setView(dialogView);

                                        builder.setPositiveButton("OK", null);
                                        //finally creating the alert dialog and displaying it
                                        AlertDialog alertDialog = builder.create();
                                        alertDialog.show();
                                        arrayList.removeAll(arrayList);
                                        Log.d("", "Alist" + arrayList);
                                        arrayAdapter = new ArrayAdapter<String>(MainSubActivity.this, android.R.layout.simple_list_item_1,
                                                android.R.id.text1, arrayList);
                                        mlist.setAdapter(arrayAdapter);
                                        hashSet.removeAll(hashSet);

                                        arrayAdapter.notifyDataSetChanged();
//                                        barcodeReader.pauseScanning();
                                        final Integer count = arrayList.size();
                                        txt.setText(count.toString());

                                     mCh.setChecked(false);
                                    }


                                });

                            } else {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pd.dismiss();


                                    }
                                });
                            }


                        } catch (final JSONException e) {
                            Log.e("", "Json parsing error: " + e.getMessage());


                        }

                    } else {
                        Log.e("", "Couldn't get json from server.");

                    }


                } catch (ClientProtocolException ce) {
                    Log.d("", "Protocol Exception" + ce.toString());

                } catch (IOException ie) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                        pd.dismiss();
                            AlertDialog dialog = new AlertDialog.Builder(MainSubActivity.this).create();
                            dialog.setTitle("NoConnection");
                            dialog.setMessage("Please check yur internet");
                            dialog.show();
                        }
                    });
                    Log.d("", "IOException" + ie.toString());
                }

//               }
            }
        }).start();
    }
        else {
            pd.dismiss();
            mBtn.setClickable(false);

        }

    }





}





