package mm.com.sweet.royal;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import mm.com.sweet.royal.network.UpdateCheckerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
EditText mEdittext,mEdittesta;
Button mBtn;

    JSONArray userdata = new JSONArray();
private String userid,type;
    private String branch,uname;
    private String city;
private String status;



    JSONArray id = new JSONArray();
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEdittext=(EditText) findViewById(R.id.editTextUsername);
        mEdittesta=(EditText) findViewById(R.id.editTextPassword);

        mBtn=(Button)findViewById(R.id.button1);
        checkAndRequestExterStoragePermission();
        getSupportActionBar().hide();


        final FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkNewVersion();

//
            }
        });

    }
    private void checkAndRequestExterStoragePermission() {
        MarshMallowPermissionUtils permissionUtils = new MarshMallowPermissionUtils(this);
        if ((!permissionUtils.checkPermissionForExternalStorage()) || (!permissionUtils.checkPermissionForCamera()) ||
                (!permissionUtils.checkPermissionForRecord())) {
            //  permissionUtils.requestPermissionForExternalStorage();
            permissionUtils.requestPermissionForAll();
        }
    }
    private void checkNewVersion() {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.label_wait),
                getString(R.string.message_checking_new_version));

        UpdateCheckerService updateCheckerService = NetworkProvider
                .getRestAdapter(Config.APK_BASE_URL).create(UpdateCheckerService.class);
        Call<CheckUpdateResponse> call = updateCheckerService.checkUpdate();
        call.enqueue(new Callback<CheckUpdateResponse>() {
            @Override
            public void onResponse(Call<CheckUpdateResponse> call, Response<CheckUpdateResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    CheckUpdateResponse checkUpdateResponse = response.body();
                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager()
                                .getPackageInfo(getPackageName(), 0);
                        final int currentVersionCode = pInfo.versionCode;
                        if (checkUpdateResponse.getAppVersion() > currentVersionCode) {
                            showMessage(getString(R.string.message_start_downloading));
                            downloadNewVersion(Config.APK_DOWNLOAD_URL, Config.APK_FILE_NAME);
                        } else {
                            String latestMsg = getString(R.string.message_up_to_date);
                            latestMsg = String.format(latestMsg, pInfo.versionName);
                            showMessage(latestMsg);
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                        showMessage(getString(R.string.error_unable_to_check_version));
                    }
                } else {
                    showMessage(getString(R.string.error_unable_to_check_version));
                }
            }

            @Override
            public void onFailure(Call<CheckUpdateResponse> call, Throwable t) {
                hideProgressDialog();
                showMessage(getString(R.string.error_unable_to_check_version));
            }
        });
    }

    private void downloadNewVersion(String url, String fileName) {
        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        //Delete update file if exists
        final File file = new File(destination);

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(getString(R.string.label_downloading_update));
        request.setTitle(getString(R.string.app_name));

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                Intent install = new Intent(Intent.ACTION_VIEW);
                install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (Build.VERSION.SDK_INT >= 24) { // Nougat and above
                    Uri apkURI = FileProvider.getUriForFile(
                            MainActivity.this,
                            getApplicationContext().getPackageName() + ".provider", file);
                    install.setDataAndType(apkURI, "application/vnd.android.package-archive");
                    install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                } else {
                    install.setDataAndType(uri, "application/vnd.android.package-archive");
                }
                startActivity(install);
                unregisterReceiver(this);
                finish();
            }
        };
        //register receiver for when .apk download is compete
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }
    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
    public void Login(View v){
        final String username=mEdittext.getText().toString();
        final String password=mEdittesta.getText().toString();

          Log.d("","Username"+username);
        Log.d("","Password"+password);

        final ProgressDialog pd=new ProgressDialog(MainActivity.this);
        pd.setTitle("Please Wait");
        pd.setMessage("Connecting to Server");
        pd.show();
        new Thread(new Runnable() {
            @Override


            public void run() {
                HttpClient httpclient=new DefaultHttpClient();
                HttpPost httpGet=new HttpPost("http://royalx.biz/cargo/api/login");
                List<NameValuePair> postData=new ArrayList<NameValuePair>(2);
                postData.add(new BasicNameValuePair("username",username));
                postData.add(new BasicNameValuePair("password",password));
////{"success":0}


                Log.d("","Name Value Pair"+postData);
                try{
                    httpGet.setEntity(new UrlEncodedFormEntity(postData));
                }catch (UnsupportedEncodingException e){
                    e.printStackTrace();
                }
                try{
                    HttpResponse response=httpclient.execute(httpGet);
                    HttpEntity entity=response.getEntity();
                   String jsonstr= EntityUtils.toString(entity,"UTF-8");
                    Log.d("","Response"+jsonstr);

                    if (jsonstr != null) {
                        try {
                            JSONObject uid=new JSONObject(jsonstr);
                            userid=uid.getString("user_id");
                            type=uid.getString("type");

                            branch=uid.getString("branch");
                            city=uid.getString("city");
                            status=uid.getString("success");
                            uname=uid.getString("username");
                            Log.d("","Status"+status);
//                            if(status != "0"){

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        pd.dismiss();



                                        Integer id = Integer.parseInt(type);
                                        if( id.equals(2)) {
                                            pd.dismiss();

                                            Intent in = new Intent(MainActivity.this, Main2Activity.class);
                                            in.putExtra("ID", userid);
                                            in.putExtra("branch", branch);
                                            in.putExtra("city", city);
                                            in.putExtra("username", uname);
                                            in.putExtra("type", type);

                                            startActivity(in);
                                        }
//
                       else if( id.equals(3)) {
                                            pd.dismiss();

                                            Intent in = new Intent(MainActivity.this, Main2Activity.class);
                                            in.putExtra("ID", userid);
                                            in.putExtra("branch", branch);
                                            in.putExtra("city", city);
                                            in.putExtra("username", uname);
                                            in.putExtra("type", type);

                                            startActivity(in);
                                        }

                                        else{
                                            pd.dismiss();

                                            Intent in=new Intent(MainActivity.this,InChargeTaskActivity.class);
                                            in.putExtra("ID",userid);
                                            in.putExtra("branch",branch);
                                            in.putExtra("city",city);
                                            in.putExtra("username",uname);
                                            in.putExtra("type",type);

                                            startActivity(in);
                                        }

                                    }
                                });





                        } catch (final JSONException e) {
                            Log.e("", "Json parsing error: " + e.getMessage());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pd.dismiss();
                                            AlertDialog dialog=new AlertDialog.Builder(MainActivity.this).create();
//                                        dialog.setTitle("Error");
                                            dialog.setMessage("User Not Found");
                                            dialog.show();


                                        }
                                    });
//                                    Toast.makeText(getApplicationContext(),
//                                            "Json parsing error: " + e.getMessage(),
//                                            Toast.LENGTH_LONG).show();
                                }
                            });

                        }

                    } else {
                        Log.e("", "Couldn't get json from server.");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        "Couldn't get json from server. Check LogCat for possible errors!",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }


                } catch(ClientProtocolException ce){
                    Log.d("","Protocol Exception"+ce.toString());

                }catch(IOException ie){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            AlertDialog dialog=new AlertDialog.Builder(MainActivity.this).create();
                            dialog.setTitle("NoConnection");
                            dialog.setMessage("Please check your internet");
                            dialog.show();
                        } 
                    });
                    Log.d("","IOException"+ie.toString());
                }


            }
        }).start();
    }

}
//ygnmain
//12345678


// network_security_config.xml
//Create xml under res directory and then network_security_config.xml in XML folder
//
//{
//    "user_id": 1,
//    "branch": "YGN_MAIN_SOKA",
//    "username": "ygnmain",
//    "city": "YGN",
//    "type": "main-branch",
//    "success": 1
//}



//{
//	"username":"5",
//	"password":"87654321"
//}