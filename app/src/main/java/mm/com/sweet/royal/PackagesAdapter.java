package mm.com.sweet.royal;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.MyViewHolder>  {


    private ArrayList<String> data;
    private ArrayList<String> data1;

    private PackagesAdapter.OnNoteListener mOnNotelistener;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTitle,mTitle1;
        RelativeLayout relativeLayout;
        PackagesAdapter.OnNoteListener onNoteListener;
        public MyViewHolder(View itemView, PackagesAdapter.OnNoteListener onNoteListener) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.txtTitle);
            mTitle1=itemView.findViewById(R.id.txtTitle1);

            this.onNoteListener=onNoteListener;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onNoteListener.OnNoteClick(getAdapterPosition());
        }
    }


    public interface OnNoteListener{
        void OnNoteClick(int position);
    }


    public PackagesAdapter(ArrayList<String> data, ArrayList<String> data1, PackagesAdapter.OnNoteListener onNoteListener) {
        this.data = data;
        this.data1=data1;
        this.mOnNotelistener=onNoteListener;
    }

    @Override
    public PackagesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.packages, parent, false);
        return new PackagesAdapter.MyViewHolder(itemView,mOnNotelistener);
    }

    @Override
    public void onBindViewHolder(PackagesAdapter.MyViewHolder holder, final int position) {
        holder.mTitle.setText("Package ID::"+data.get(position));
        holder.mTitle1.setText("Count:"+data1.get(position));

    }


    @Override
    public int getItemCount() {
        return data.size();

    }


    public void removeItem(int position) {
        data.remove(position);
        data1.remove(position);

        notifyItemRemoved(position);
    }

    public void restoreItem(String item,String item1, int position) {
        data.add(position, item);
        data1.add(position, item1);

        notifyItemInserted(position);
    }

    public ArrayList<String> getData() {
        return data;
    }
    public ArrayList<String> getData1() {
        return data1;
    }
}

