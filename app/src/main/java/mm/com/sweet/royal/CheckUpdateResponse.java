package mm.com.sweet.royal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gca on 4/9/18.
 */

public class CheckUpdateResponse {

    @SerializedName("app_version")
    private int appVersion;

    public int getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(int appVersion) {
        this.appVersion = appVersion;
    }
}
