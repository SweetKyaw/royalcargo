package mm.com.sweet.royal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.navigation.ui.AppBarConfiguration;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

public class InChargeTaskActivity extends AppCompatActivity {
    private CardView mCard1,mCard2,mCard3,mCard4,mCard5;
    private TextView mtv1,mtv2;
    private String userid,type,city;
    public final String PREFS_NAME="MyPrefsFile";
    private ProgressDialog mProgressDialog;
    private AppBarConfiguration mAppBarConfiguration;
    private GridLayout gd1,gd2,gd3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_charge_task);
        mCard1=(CardView)findViewById(R.id.cardcollect) ;
        mCard2=(CardView)findViewById(R.id.cardhandover);
        mCard3=(CardView)findViewById(R.id.cardbranchout) ;
        mCard4=(CardView)findViewById(R.id.cardReceived) ;
        mCard5=(CardView) findViewById(R.id.inchargeReceived);
//        mCard5=(CardView) findViewById(R.id.cardreport);


        gd1=(GridLayout) findViewById(R.id.gd1);
        gd2=(GridLayout) findViewById(R.id.gd2);
        gd3=(GridLayout) findViewById(R.id.gd3);


        mCard1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in1=new Intent(InChargeTaskActivity.this,CollectedActivity.class);
                in1.putExtra("ID",userid);
                in1.putExtra("type",type);
                in1.putExtra("city",city);

                startActivity(in1);
            }
        });
//
        mCard2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in2=new Intent(InChargeTaskActivity.this,InCheckHandoverActivity.class);
                in2.putExtra("city",city);

                in2.putExtra("ID",userid);

                startActivity(in2);

            }
        });


        mCard3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in3=new Intent(InChargeTaskActivity.this,Branchout.class);
                in3.putExtra("ID",userid);

                startActivity(in3);

            }
        });
        mCard4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in3=new Intent(InChargeTaskActivity.this,ReceieveActivity.class);
                in3.putExtra("ID",userid);

                startActivity(in3);

            }
        });
        mCard5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in3=new Intent(InChargeTaskActivity.this,InchargeReceiveActivity.class);
                in3.putExtra("ID",userid);

                startActivity(in3);

            }
        });

//        mRecent=(Button)findViewById(R.id.recent);

        city=getIntent().getStringExtra("city");
        userid=getIntent().getStringExtra("ID");
        type=getIntent().getStringExtra("type");
        Log.d("","Main2Activity"+type);
        Integer id = Integer.parseInt(type);
        if( id.equals(3)) {
//            mCard1.setVisibility(View.GONE);
//            mCard2.setVisibility(View.GONE);
//            mCard3.setVisibility(View.GONE);
//            mCard5.setVisibility(View.GONE);


            gd1.setVisibility(View.GONE);
            gd2.setVisibility(View.GONE);
//            mCard3.setVisibility(View.GONE);
//            mCard5.setVisibility(View.GONE);


        } else if(id.equals(4)){


//            mCard1.setVisibility(View.INVISIBLE);
//            mCard4.setVisibility(View.GONE);

            gd1.setVisibility(View.INVISIBLE);
            gd3.setVisibility(View.GONE);


        }
        else if(id.equals(5)){
//            mCard2.setVisibility(View.GONE);
//            mCard3.setVisibility(View.GONE);
//            mCard5.setVisibility(View.GONE);
//            mCard4.setVisibility(View.GONE);

            gd2.setVisibility(View.GONE);
            gd3.setVisibility(View.GONE);


        }else {
//            mCard4.setVisibility(View.GONE);
            gd3.setVisibility(View.GONE);

        }

    }
}
