package mm.com.sweet.royal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

public class OperatorMainActivity extends AppCompatActivity   {

    private CardView mCard1,mCard2,mCard3,mCard4;
    private TextView mtv1,mtv2;
    private String userid,type,city;
    public final String PREFS_NAME="MyPrefsFile";
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_main);
        mCard1=(CardView)findViewById(R.id.cardbranchin) ;
        mCard2=(CardView)findViewById(R.id.cardHandover);
        mCard3=(CardView)findViewById(R.id.cardReceived) ;
        mCard4=(CardView)findViewById(R.id.cardreport) ;


        mCard1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in1=new Intent(OperatorMainActivity.this,MainSubActivity.class);
                in1.putExtra("ID",userid);
                startActivity(in1);
            }
        });

        mCard2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in2=new Intent(OperatorMainActivity.this,HandoverActivity.class);
                in2.putExtra("city",city);

                in2.putExtra("ID",userid);

                startActivity(in2);

            }
        });


        mCard3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in3=new Intent(OperatorMainActivity.this,ReceieveActivity.class);
                in3.putExtra("ID",userid);

                startActivity(in3);

            }
        });

//        mRecent=(Button)findViewById(R.id.recent);

        city=getIntent().getStringExtra("city");
        userid=getIntent().getStringExtra("ID");
        type=getIntent().getStringExtra("type");
        Log.d("","Main2Activity"+type);
//        setVisibility(View.GONE);
        Integer id = Integer.parseInt(type);

if(id.equals(3)){
    mCard1 .setVisibility(View.GONE);
    mCard2 .setVisibility(View.GONE);

}


    }


}
